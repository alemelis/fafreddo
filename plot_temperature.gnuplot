set terminal png size 800,300
set key off
set xlabel "time"
set ylabel "temperature (C)"
set output 'plots/temperature.png'
plot "< paste data/temperature.dat data/time.dat" using 1:xticlabel(2) with lines