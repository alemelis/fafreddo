#!/bin/bash

cd /home/pi/fafreddo
python3 measure_temp.py
gnuplot plot_temperature.gnuplot
gnuplot plot_humidity.gnuplot

cp index.html /var/www/html/index.html
cp plots/temperature.png /var/www/html/imgs/temperature.png
cp plots/humidity.png /var/www/html/imgs/humidity.png
