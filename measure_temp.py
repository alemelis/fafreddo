import board
import adafruit_dht
import time
import os
import subprocess

def ask_sensor(dhtDevice):
    while True:
        try:
            temperature = float(dhtDevice.temperature)
            humidity = float(dhtDevice.humidity)
            date = subprocess.check_output("date", shell=True,
                universal_newlines=True)
            return (temperature, humidity, str(date.strip()))

        except:
            time.sleep(1.0)


def write_data(temp, hum, t):
    if not os.path.isdir("/home/pi/fafreddo/data/"):
        os.makedirs("/home/pi/fafreddo/data/")

    if not os.path.isfile("/home/pi/fafreddo/data//temperature.dat"):
        w_mod = 'w'
    else:
        w_mod = 'a'

    for d, v in zip(["temperature", "humidity", "time"], [temp, hum, t]):
        with open("/home/pi/fafreddo/data/{}.dat".format(d), w_mod) as f:
            if d == "time":
                f.write("{}\n".format(v.split(' ')[3]))
            else:
                f.write("{}\n".format(v))


def write_current_data(temp, hum, t):
    with open("/home/pi/fafreddo/data/current.txt", 'w') as f:
        f.write("{}\n".format(t))
        f.write("Temperature: {} C\n".format(temp))
        f.write("Humidity: {}%".format(hum))


def write_index():
    with open("index.html", 'w') as index_f:
        index_f.write("<html>\n<body>\n")

        with open("/home/pi/fafreddo/data/current.txt", 'r') as current_data:
            for line in current_data.readlines():
                index_f.write("<p>{}</p>\n".format(line.strip()))

        index_f.write('<img src="imgs/temperature.png"><br>\n<img src="imgs/humidity.png">\n</body>\n</html>')


if __name__ == "__main__":
    dhtDevice = adafruit_dht.DHT11(board.D4)
    temp, hum, t = ask_sensor(dhtDevice)
    write_data(temp, hum, t)
    write_current_data(temp, hum, t)
    write_index()
