set terminal png size 800,300
set key off
set xlabel "time"
set ylabel "humidity (%)"
set output 'plots/humidity.png'
plot "< paste data/humidity.dat data/time.dat" using 1:xticlabel(2) with lines