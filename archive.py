import subprocess
from shutils import move
import os


def move_to_archive():
	if not os.path.isdir("/home/pi/fafreddo/data/archive"):
		os.makedirs("/home/pi/fafreddo/data//archive")

	date = subprocess.check_output("date", shell=True,
                universal_newlines=True)
	today = ''.join(date.strip().split(' ')[:5])

	for f in ["temperature", "humidity", "time"]:
		move("/home/pi/fafreddo/data//{}.dat".format(f),
			"/home/pi/fafreddo/data/archive/{}-{}.dat".format(today, f))


if __name__ == "__main__":
	move_to_archive()